# Unreal StudyFramework Plugin

This Plugin allows the for the quick setting up and execution of factorial-design studies.

Look at the [Wiki](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/unreal-study-framework/-/wikis/home) for all information on usage etc.

An en example of setting up a study can be found at [https://www.youtube.com/watch?v=7LfJkk7MOn8](https://www.youtube.com/watch?v=7LfJkk7MOn8)

If you use this for a scientific work, please cite:<br>
> J. Ehret, A. Bönsch, J. Fels, S. J. Schlittmeier, and T. W. Kuhlen. StudyFramework: Comfortably Setting up and Conducting Factorial-Design Studies Using the Unreal Engine. In *2024 IEEE Conference on Virtual Reality and 3D User Interfaces Abstracts and Workshops (VRW): Workshop ”Open Access Tools and Libraries for Virtual Reality”*, 2024

If you want to use it or have questions, feel free to contact Jonathan Ehret (ehret@vr.rwth-aachen.de) or info@vr.rwth-aachen.de

The StudyFramework is published under a *BSD 3-Clause License*