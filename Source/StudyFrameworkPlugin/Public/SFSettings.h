#pragma once
#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "Engine/DeveloperSettings.h"
#include "SFSettings.generated.h"

UCLASS(config = Engine, defaultconfig, meta = (DisplayName = "StudyFramework"))
class USFSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, config, Category = "General", meta = (DisplayName = "Absolute Path to eye calibration executable"))
	FString EyeCalibrationExePath = "C:/Program Files/HP/HP Omnicept/HP Omnicept Eye Tracking Calibration/ETCal/Binaries/Win64/ETCal-Win64-Shipping.exe";
};
