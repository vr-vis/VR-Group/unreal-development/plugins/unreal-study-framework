// Copyright 2015 Moritz Wundke. All Rights Reserved.
// Released under MIT

#pragma once


#include "SFFadeVisualizer.generated.h"

/**
 * A simple UGameViewportClient used to handle a global fade in/out
 * Always starts faded out
 * It had to be derived from UDisplayClusterViewportClient instead of UGameViewportClient so it works in nDisplay
 */

UCLASS()
class STUDYFRAMEWORKPLUGIN_API USFFadeVisualizer : public UObject, public FTickableGameObject
{
	GENERATED_BODY()

public:

	void Tick(float DeltaTime) override;
	bool IsTickable() const override;
	bool IsTickableInEditor() const override;
	bool IsTickableWhenPaused() const override;
	ETickableTickType GetTickableTickType() const override;
	TStatId GetStatId() const override;

	/** Clear fading state */
	void ClearFade();

	/** Used for Fade to and from black
	* Call with Duration = 0.0f if you want it e.g. to stay faded out (default starting state)
	*/
	void Fade(const float Duration, const bool bToBlackN, const FLinearColor FadeColorN = FLinearColor::Black);

	/** Get how many seconds are left */
	float FadeTimeRemaining();

	/** Does the actual screen fading */
	void DrawScreenFade();

private:

	// Values used by our screen fading
	// these default values result in a faded out start on intialization of a viewport
	bool bFading = false;
	bool bToBlack = true;
	double FadeStartTime = 0.0;
	float FadeDuration = 0.0f;
	FLinearColor FadeColor = FLinearColor::Black;
};
