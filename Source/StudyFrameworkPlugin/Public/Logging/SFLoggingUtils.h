// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Core.h"
#include "SFParticipant.h"

class FJsonObject;

class FSFLoggingUtils
{
public:

	// Log into Console and errors also on Screen, works also in Shipping build in contrast to UE_LOG
	static void Log(FString Text, bool Error = false);

	static void SetupParticipantLoggingStream(const FString& ParticipantInfix);

	static void OnSessionEnd(const bool);

private:

	// To setup the debugging logs to be used
	static void SetupDebugLoggingStreams();

	static bool bDebugLogsSetUp;
};

