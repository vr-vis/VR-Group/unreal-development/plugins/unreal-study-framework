#include "SFIndependentVariable.h"

#include "Dom/JsonObject.h"
#include "Help/SFUtils.h"


USFIndependentVariable::USFIndependentVariable()
{
}

TSharedPtr<FJsonObject> USFIndependentVariable::GetAsJson() const
{
	TSharedPtr<FJsonObject> Json = MakeShared<FJsonObject>();
	Json->SetStringField(TEXT("Name"), Name);
	Json->SetStringField(TEXT("ValType"), GetValTypeAsString());
	Json->SetBoolField(TEXT("AskedAtBeginning"), bAskedAtBeginning);
	Json->SetStringField(TEXT("Prompt"), Prompt);

	TArray<TSharedPtr<FJsonValue>> OptionsJson;
	for (const FString& Option : Options)
	{
		TSharedRef<FJsonValueString> JsonValue = MakeShared<FJsonValueString>(Option);
		OptionsJson.Add(JsonValue);
	}
	Json->SetArrayField("Options", OptionsJson);

	return Json;
}

void USFIndependentVariable::FromJson(TSharedPtr<FJsonObject> Json)
{
	Name = Json->GetStringField(TEXT("Name"));
	SetValTypeFromString(Json->GetStringField(TEXT("ValType")));
	bAskedAtBeginning = Json->GetBoolField(TEXT("AskedAtBeginning"));
	Prompt = Json->GetStringField(TEXT("Prompt"));

	Options.Empty();
	TArray<TSharedPtr<FJsonValue>> OptionsJson = Json->GetArrayField(TEXT("Options"));
	for (auto Option : OptionsJson) {
		Options.Add(Option->AsString());
	}
}

FString USFIndependentVariable::GetValTypeAsString() const
{
	switch (ValueType) {
	default:
	case EValType::TEXT:
		return "Text";
	case EValType::MULTIPLECHOICE:
		return "Multiple Choice";
	}
}

void USFIndependentVariable::SetValTypeFromString(const FString& Str)
{
	if (Str == "Text") {
		ValueType = EValType::TEXT;
	}
	else if (Str == "Multiple Choice") {
		ValueType = EValType::MULTIPLECHOICE;
	}
}

#if WITH_EDITOR
bool USFIndependentVariable::CanEditChange(const FProperty* InProperty) const
{
	if (InProperty->GetFName() == "Options" && ValueType == EValType::TEXT)
	{
		return false;
	}
	return true;
}
#endif
