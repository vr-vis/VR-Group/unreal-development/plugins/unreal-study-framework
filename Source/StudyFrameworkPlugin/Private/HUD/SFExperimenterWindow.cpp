// Fill out your copyright notice in the Description page of Project Settings.

#include "HUD/SFExperimenterWindow.h"
#include "HUD/SFHMDSpectatorHUDHelp.h"

#include "Dom/JsonObject.h"
#include "Slate/SceneViewport.h"
#include "Widgets/SViewport.h"


TSharedPtr<FJsonObject> FExperimenterViewConfig::GetAsJson() const
{
	TSharedPtr<FJsonObject> Json = MakeShareable(new FJsonObject());
	Json->SetBoolField(TEXT("ShowHUD"), bShowHUD);
	Json->SetBoolField(TEXT("ShowConditionsPanelByDefault"), bShowConditionsPanelByDefault);
	Json->SetBoolField(TEXT("ShowExperimenterViewInSecondWindow"), bShowExperimenterViewInSecondWindow);
	Json->SetNumberField(TEXT("SecondWindowSizeX"), SecondWindowSize.X);
	Json->SetNumberField(TEXT("SecondWindowSizeY"), SecondWindowSize.Y);
	Json->SetNumberField(TEXT("SecondWindowPosX"), SecondWindowPos.X);
	Json->SetNumberField(TEXT("SecondWindowPosY"), SecondWindowPos.Y);
	return Json;
}

void FExperimenterViewConfig::FromJson(TSharedPtr<FJsonObject> Json)
{

	bShowHUD = Json->GetBoolField(TEXT("ShowHUD"));
	bShowConditionsPanelByDefault = Json->GetBoolField(TEXT("ShowConditionsPanelByDefault"));
	bShowExperimenterViewInSecondWindow = Json->GetBoolField(TEXT("ShowExperimenterViewInSecondWindow"));
	SecondWindowSize = FVector2D(Json->GetNumberField(TEXT("SecondWindowSizeX")), Json->GetNumberField(TEXT("SecondWindowSizeY")));
	SecondWindowPos = FVector2D(Json->GetNumberField(TEXT("SecondWindowPosX")), Json->GetNumberField(TEXT("SecondWindowPosY")));
}

void USFExperimenterWindow::CreateWindow(FExperimenterViewConfig Config)
{
	SecondWindow = SNew(SWindow)
	.AutoCenter(EAutoCenter::None)
	.Title(FText::FromString(TEXT("Experimenter View")))
	.IsInitiallyMaximized(false)
	.ScreenPosition(Config.SecondWindowPos)
	.ClientSize(Config.SecondWindowSize)
	.CreateTitleBar(true)
	.SizingRule(ESizingRule::UserSized)
	.SupportsMaximize(true)
	.SupportsMinimize(true)
	.HasCloseButton(true);

	FSlateApplication & SlateApp = FSlateApplication::Get();

	SlateApp.AddWindow(SecondWindow.ToSharedRef(), true);

	// add the viewport of the primary window :-)
	SecondWindow->SetContent(ASFHMDSpectatorHUDHelp::GetSceneViewport()->GetViewportWidget().Pin().ToSharedRef());

	//add overlay where we attach HUD widget to later on
	HUDWidgetOverlay = SecondWindow->AddOverlaySlot().GetSlot();
}

void USFExperimenterWindow::DestroyWindow()
{
	SecondWindow->RequestDestroyWindow();
	SecondWindow.Reset();
	Viewport.Reset();

}

void USFExperimenterWindow::AddHUDWidget(TSharedRef<SWidget>& Widget)
{
	HUDWidgetOverlay->DetachWidget();
	HUDWidgetOverlay->AttachWidget(Widget);
}


