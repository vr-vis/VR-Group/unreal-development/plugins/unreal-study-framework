// Copyright 2015 Moritz Wundke. All Rights Reserved.
// Released under MIT

#include "HUD/SFFadeVisualizer.h"
#include "Help/SFUtils.h"
#include "Logging/SFLoggingUtils.h"
#include "SFGameInstance.h"
#include "Kismet/GameplayStatics.h"


void USFFadeVisualizer::Tick(float DeltaTime)
{
	if (bFading)
	{
		DrawScreenFade();
	}
}
bool USFFadeVisualizer::IsTickable() const
{
	return true;
}

bool USFFadeVisualizer::IsTickableInEditor() const
{
	return false;
}

bool USFFadeVisualizer::IsTickableWhenPaused() const
{
	return false;
}

ETickableTickType USFFadeVisualizer::GetTickableTickType() const
{
	return ETickableTickType::Always;
}
TStatId USFFadeVisualizer::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(USFFadeVisualizer, STATGROUP_Tickables);
}

void USFFadeVisualizer::ClearFade()
{
	bFading = false;
}

void USFFadeVisualizer::Fade(const float Duration, const bool bToBlackN, const FLinearColor FadeColorN)
{
	bFading = true;
	bToBlack = bToBlackN;
	FadeDuration = Duration;
	FadeStartTime = FPlatformTime::Seconds();
	FadeColor = FadeColorN;

	DrawScreenFade();
}

float USFFadeVisualizer::FadeTimeRemaining()
{
	if (bFading)
	{
		const double Time = FPlatformTime::Seconds();
		const float TimeLeft = FadeDuration - (Time - FadeStartTime);
		if (TimeLeft > 0.0f)
		{
			return TimeLeft;
		}
	}
	return 0.0f;
}

void USFFadeVisualizer::DrawScreenFade()
{
	if (!bFading)
	{
		return;
	}

	float Progress = 1.0f; //goes from 0.0 to 1.0 over a fade
	if (FadeDuration > 0.0f)
	{
		Progress = 1.0f - (FadeTimeRemaining() / FadeDuration);
	}

	Progress = FMath::Clamp(Progress, 0.0f, 1.0f);

	float FadeAlpha = Progress;
	if (!bToBlack) {
		FadeAlpha = 1.0f - Progress;
	}

	APlayerCameraManager* CameraManager = UGameplayStatics::GetPlayerCameraManager(USFGameInstance::Get()->GetWorld(), 0);
	if (CameraManager) {
		CameraManager->SetManualCameraFade(FadeAlpha, FadeColor, false);
	}

	if (Progress >= 1.0f && !bToBlack) {
		ClearFade(); //fading is done
		//only "stop fading" when we are fading in, otherwise we stay faded out!
	}

}

