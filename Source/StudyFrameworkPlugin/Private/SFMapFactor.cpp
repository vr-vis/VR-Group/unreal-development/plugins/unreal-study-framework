#include "SFMapFactor.h"

USFMapFactor::USFMapFactor()
{
}

TSharedPtr<FJsonObject> USFMapFactor::GetAsJson() const
{
	TSharedPtr<FJsonObject> Json = Super::GetAsJson();

	Json->SetBoolField("MapFactor", true);

	return Json;
}

void USFMapFactor::FromJson(TSharedPtr<FJsonObject> Json)
{
	Super::FromJson(Json);

	for(FString LevelName : Levels)
	{
		// we need to transform /Path/To/Asset into /Path/To/Asset.Asset;
		TArray<FString> Parts;
		LevelName.ParseIntoArray(Parts,TEXT("/"),true);
		FString LevelNameFull = LevelName + "." + Parts[Parts.Num()-1];
		
		FSoftObjectPath LevelPath(LevelNameFull);
		TSoftObjectPtr<UWorld> LevelObj(LevelPath);
		LevelObj.ToSoftObjectPath().PostLoadPath(nullptr);
		Maps.Add(LevelObj);
	}
}

#if WITH_EDITOR
bool USFMapFactor::CanEditChange(const FProperty* InProperty) const
{
	// just use this to not allow editing "Levels" directly at all
	if (InProperty->GetFName() == "Levels")
	{
		return false;
	}
	return Super::CanEditChange(InProperty);
}

void USFMapFactor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if(PropertyChangedEvent.MemberProperty->GetFName() == "Maps")
	{
		//adapt levels accordingly, since that is what is internaly used
		Levels.Empty();
		for(TSoftObjectPtr<UWorld> Map : Maps)
		{
			// we want to store the name as /Path/To/Asset not as /Path/To/Asset.Asset
			// this is beneficial for readability and also needed for loading the levels as is now
			FSoftObjectPath Path = Map.ToSoftObjectPath();
			FString PathString = Path.GetAssetPathString();
			int32 DotIndex;
			PathString.FindLastChar('.', DotIndex);
			PathString.LeftChopInline(PathString.Len() - DotIndex);
			Levels.Add(PathString);
		}
	}
}
#endif

