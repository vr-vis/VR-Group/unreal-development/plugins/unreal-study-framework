// Fill out your copyright notice in the Description page of Project Settings.


#include "SFParticipant.h"


#include "IUniversalLogging.h"
#include "SFGameInstance.h"
#include "HAL/FileManagerGeneric.h"
#include "Help/SFUtils.h"
#include "Logging/SFLoggingBPLibrary.h"
#include "Logging/SFLoggingUtils.h"
#include "JsonUtilities.h"

USFParticipant::USFParticipant()
{
}

USFParticipant::~USFParticipant()
{
}

bool USFParticipant::Initialize(int SequenceNumber, FString ID)
{
	ParticipantSequenceNumber = SequenceNumber;
	ParticipantID = ID;

	StartTime = FPlatformTime::Seconds();
	IDisplayClusterClusterManager* ClusterManager = IDisplayCluster::Get().GetClusterMgr();
	if (ClusterManager && !ClusterEventListenerDelegate.IsBound())
	{
		ClusterEventListenerDelegate = FOnClusterEventJsonListener::CreateUObject(
			this, &USFParticipant::HandleClusterEvent);
		ClusterManager->AddClusterEventJsonListener(ClusterEventListenerDelegate);
	}
	ParticipantLoggingInfix = "LogParticipant-" + ParticipantID + "_" + FDateTime::Now().ToString();
	FSFLoggingUtils::SetupParticipantLoggingStream(ParticipantLoggingInfix);

	return true;
}

void USFParticipant::SetStudyConditions(TArray<USFCondition*> NewConditions)
{
	Conditions = NewConditions;

	// Create initial Json file
	GenerateExecutionJsonFile();
}

void USFParticipant::GenerateExecutionJsonFile() const
{
	TSharedPtr<FJsonObject> Json = MakeShared<FJsonObject>();

	Json->SetStringField(TEXT("ParticipantID"), ParticipantID);
	Json->SetNumberField(TEXT("ParticipantSequenceNumber"), ParticipantSequenceNumber);

	TArray<TSharedPtr<FJsonValue>> ConditionsArray;
	for (auto Condition : Conditions)
	{
		TSharedRef<FJsonValueObject> JsonValue = MakeShared<FJsonValueObject>(Condition->GetAsJson());
		ConditionsArray.Add(JsonValue);
	}
	Json->SetArrayField(TEXT("Conditions"), ConditionsArray);

	TArray<TSharedPtr<FJsonValue>> IndependentArray;
	for (auto Entry : IndependentVariablesValues)
	{
		TSharedPtr<FJsonObject> JsonVar = Entry.Key->GetAsJson();
		JsonVar->SetStringField(TEXT("Value"), Entry.Value);
		IndependentArray.Add(MakeShared<FJsonValueObject>(JsonVar));
	}
	Json->SetArrayField(TEXT("Independent Variables"), IndependentArray);

	FSFUtils::WriteJsonToFile(Json, "StudyRuns/Participant_" + ParticipantID + ".txt");
}

void USFParticipant::UpdateIndependentVarsExecutionJsonFile() const
{
	if (ParticipantSequenceNumber == -1)
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::UpdateIndependentVarsExecutionJsonFile] participant json file for participant with sequence number "
			+
			FString::FromInt(ParticipantSequenceNumber) +
			" is not to be read, probably called on init so everything is fine!", false);
		return;
	}
	TSharedPtr<FJsonObject> Json = FSFUtils::ReadJsonFromFile(
		"StudyRuns/Participant_" + ParticipantID + ".txt");
	if (Json == nullptr)
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::UpdateIndependentVarsExecutionJsonFile] participant json file for participant " +
			ParticipantID + " cannot be read!", false);
		return;
	}

	TArray<TSharedPtr<FJsonValue>> IndependentArray;
	for (auto Entry : IndependentVariablesValues)
	{
		TSharedPtr<FJsonObject> JsonVar = Entry.Key->GetAsJson();
		JsonVar->SetStringField(TEXT("Value"), Entry.Value);
		IndependentArray.Add(MakeShared<FJsonValueObject>(JsonVar));
	}
	Json->RemoveField(TEXT("Independent Variables"));
	Json->SetArrayField(TEXT("Independent Variables"), IndependentArray);

	FSFUtils::WriteJsonToFile(Json, "StudyRuns/Participant_" + ParticipantID + ".txt");
}

void USFParticipant::ReadExecutionJsonFile(FString ParticipantID, TArray<USFCondition*>& Conditions_Out,
                                           TMap<USFIndependentVariable*, FString>& IndependentVariablesValues_Out)
{
	if (ParticipantID.IsEmpty())
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::ReadExecutionJsonFile] participant json file for participant " +
			ParticipantID + " is not to be read, probably called on init so everything is fine!", false);
		return;
	}
	TSharedPtr<FJsonObject> Json = FSFUtils::ReadJsonFromFile(
		"StudyRuns/Participant_" + ParticipantID + ".txt");
	TArray<USFCondition*> LoadedConditions;
	if (Json == nullptr)
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::ReadExecutionJsonFile] participant json file for participant " +
			ParticipantID + " cannot be read!", true);
		return;
	}

	TArray<TSharedPtr<FJsonValue>> ConditionsArray = Json->GetArrayField(TEXT("Conditions"));
	for (TSharedPtr<FJsonValue> ConditionJson : ConditionsArray)
	{
		USFCondition* Condition = NewObject<USFCondition>();
		Condition->FromJson(ConditionJson->AsObject());
		LoadedConditions.Add(Condition);
	}
	Conditions_Out = LoadedConditions;

	IndependentVariablesValues_Out.Empty();
	TArray<TSharedPtr<FJsonValue>> IndependentArray = Json->GetArrayField(TEXT("Independent Variables"));
	for (TSharedPtr<FJsonValue> VarJson : IndependentArray)
	{
		USFIndependentVariable* Var = NewObject<USFIndependentVariable>();
		Var->FromJson(VarJson->AsObject());
		FString Val = VarJson->AsObject()->GetStringField(TEXT("Value"));
		IndependentVariablesValues_Out.Add(Var, Val);
	}
}

FString USFParticipant::GetCurrentTimeAsString() const
{
	return FString::Printf(TEXT("%.3f"), GetCurrentTime());
}

float USFParticipant::GetCurrentTime() const
{
	return FPlatformTime::Seconds() - StartTime;
}

void USFParticipant::StoreInPhaseLongTable() const
{
	USFCondition* CurrCondition = GetCurrentCondition();

	FString Filename = FPaths::ProjectDir() + "StudyFramework/StudyLogs/Phase_" + CurrCondition->PhaseName + ".csv";

	if (!FPaths::FileExists(Filename))
	{
		FString Header = "ParticipantID";
		for (auto IV : IndependentVariablesValues)
		{
			Header += "," + IV.Key->Name;
		}
		Header += ",Phase";
		for (auto Factor : CurrCondition->FactorLevels)
		{
			Header += "," + Factor.Key;
		}
		for (auto Var : CurrCondition->DependentVariables)
		{
			if (Cast<USFMultipleTrialDependentVariable>(Var))
			{
				//those are stored in their own files
				continue;
			}
			Header += "," + Var->Name;
		}
		Header += ",Time\n";
		FFileHelper::SaveStringToFile(*Header, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM);
	}

	FString ConditionResults = ParticipantID;
	for (auto IV : IndependentVariablesValues)
	{
		ConditionResults += "," + IV.Value;
	}
	ConditionResults += "," + CurrCondition->PhaseName;
	for (auto Factor : CurrCondition->FactorLevels)
	{
		ConditionResults += "," + Factor.Value;
	}
	for (auto Var : CurrCondition->DependentVariables)
	{
		if (Cast<USFMultipleTrialDependentVariable>(Var))
		{
			//those are stored in their own files
			continue;
		}
		ConditionResults += "," + Var->Value;
	}
	ConditionResults += "," + FString::Printf(TEXT("%.2f"), CurrCondition->GetTimeTaken());
	//append this
	ConditionResults += "\n";
	FFileHelper::SaveStringToFile(*ConditionResults, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM,
	                              &IFileManager::Get(), FILEWRITE_Append);
}

void USFParticipant::StoreTrialInTrialDVLongTable(USFMultipleTrialDependentVariable* DependentVariable,
                                                  TArray<FString> Values) const
{
	USFCondition* CurrCondition = GetCurrentCondition();
	FString Filename = FPaths::ProjectDir() + "StudyFramework/StudyLogs/Phase_" + CurrCondition->PhaseName + "_" +
		DependentVariable->Name + ".csv";

	if (!FPaths::FileExists(Filename))
	{
		FString Header = "ParticipantID";
		for (auto IV : IndependentVariablesValues)
		{
			Header += "," + IV.Key->Name;
		}
		Header += ",Phase";
		for (auto Factor : CurrCondition->FactorLevels)
		{
			Header += "," + Factor.Key;
		}
		Header += ",Trial";
		for (const FString& SubName : DependentVariable->SubVariableNames)
		{
			Header += "," + SubName;
		}
		Header += "\n";
		FFileHelper::SaveStringToFile(*Header, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM);
	}

	FString TrialResult = ParticipantID;
	for (auto IV : IndependentVariablesValues)
	{
		TrialResult += "," + IV.Value;
	}
	TrialResult += "," + CurrCondition->PhaseName;
	for (auto Factor : CurrCondition->FactorLevels)
	{
		TrialResult += "," + Factor.Value;
	}
	TrialResult += "," + FString::FromInt(DependentVariable->Values.size());
	for (const FString& Value : Values)
	{
		TrialResult += "," + Value;
	}

	//append this
	TrialResult += "\n";
	FFileHelper::SaveStringToFile(*TrialResult, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM,
	                              &IFileManager::Get(), FILEWRITE_Append);
}

void USFParticipant::StoreInIndependentVarLongTable() const
{
	FString Filename = FPaths::ProjectDir() + "StudyFramework/StudyLogs/IndependentVariables.csv";

	if (!FPaths::FileExists(Filename))
	{
		FString Header = "ParticipantID";
		for (auto Var : IndependentVariablesValues)
		{
			Header += "," + Var.Key->Name;
		}
		Header += "\n";
		FFileHelper::SaveStringToFile(*Header, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM);
	}

	FString VarValues = ParticipantID;
	for (auto Var : IndependentVariablesValues)
	{
		VarValues += "," + Var.Value;
	}

	FString StartStringSearch = ParticipantID + ",";
	TArray<FString> ExistingLines;
	FFileHelper::LoadFileToStringArray(ExistingLines, *Filename);
	int found = -1;
	for (int i = 0; i < ExistingLines.Num(); i++)
	{
		if (ExistingLines[i].StartsWith(StartStringSearch))
		{
			found = i;
			break;
		}
	}
	if (found != -1)
	{
		ExistingLines[found] = VarValues;
		FString ToSave = "";
		for (int i = 0; i < ExistingLines.Num(); i++)
		{
			ToSave += ExistingLines[i] + "\n";
		}
		FFileHelper::SaveStringToFile(ToSave, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM);
	}
	else
	{
		VarValues += "\n";
		FFileHelper::SaveStringToFile(*VarValues, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM,
		                              &IFileManager::Get(), FILEWRITE_Append);
	}
}

void USFParticipant::DeleteStoredDataForConditionFromLongTable(const USFCondition* Condition)
{
	const FString Filename = FPaths::ProjectDir() + "StudyFramework/StudyLogs/Phase_" + Condition->PhaseName + ".csv";
	RemoveLinesOfConditionAndWriteToFile(Condition, Filename);
}

void USFParticipant::DeleteStoredTrialDataForCondition(const USFCondition* Condition,
                                                       USFMultipleTrialDependentVariable* DependentVariable)
{
	const FString Filename = FPaths::ProjectDir() + "StudyFramework/StudyLogs/Phase_" + Condition->PhaseName + "_" +
		DependentVariable->Name + ".csv";
	RemoveLinesOfConditionAndWriteToFile(Condition, Filename);
}

void USFParticipant::RemoveLinesOfConditionAndWriteToFile(const USFCondition* Condition, const FString Filename)
{
	TArray<FString> Lines;
	TArray<FString> CleanedLines;
	if (!FFileHelper::LoadFileToStringArray(Lines, *Filename))
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::RemoveLinesOfConditionAndWriteToFile] Cannot read file: " + Filename +
			" (probably there was no data recorded yet).", false);
		return;
	}

	TArray<FString> HeaderEntries;
	if (Lines.Num() > 0)
	{
		Lines[0].ParseIntoArray(HeaderEntries, TEXT(","), false);
	}
	CleanedLines.Add(Lines[0]);

	bool bHasRemovedLines = false;
	for (int i = 1; i < Lines.Num(); ++i)
	{
		TArray<FString> Entries;
		Lines[i].ParseIntoArray(Entries, TEXT(","), false);

		if (Entries.Num() > 0 && Entries[0] == ParticipantID)
		{
			//so this is a line of this participant, but also from the right condition?
			bool bRightCondition = true;
			for (auto FactorLevel : Condition->FactorLevels)
			{
				if (Entries[HeaderEntries.Find(FactorLevel.Key)] != FactorLevel.Value)
				{
					//This line does not belong to this condition
					bRightCondition = false;
				}
			}
			if (!bRightCondition)
			{
				CleanedLines.Add(Lines[i]);
			}
			//At least this line will be removed
			else if (!bHasRemovedLines)
			{
				bHasRemovedLines = true;
			}
		}
		else
		{
			//from another participant so just keep this line!
			CleanedLines.Add(Lines[i]);
		}
	}

	if (bHasRemovedLines)
	{
		CreateLongTableBackUp(Filename);
	}

	FFileHelper::SaveStringArrayToFile(CleanedLines, *Filename, FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM,
	                                   &IFileManager::Get(), FILEWRITE_None);
}

bool USFParticipant::StartStudy()
{
	// Set first condition
	CurrentConditionIdx = -1;

	USFLoggingBPLibrary::LogComment("Start Study for ParticipantID: " + ParticipantID);

	return true;
}

void USFParticipant::EndStudy()
{
	USFLoggingBPLibrary::LogComment("EndStudy");
	LogCurrentParticipant();
	StoreInPhaseLongTable();
	//save also inpdependent variables if something is configured during the study
	StoreInIndependentVarLongTable();
}

USFCondition* USFParticipant::GetCurrentCondition() const
{
	if (CurrentConditionIdx >= 0 && CurrentConditionIdx < Conditions.Num())
	{
		return Conditions[CurrentConditionIdx];
	}

	return nullptr;
}

TOptional<int> USFParticipant::GetNextConditionIndex() const
{
	// Get next Condition
	if (CurrentConditionIdx + 1 >= Conditions.Num())
	{
		return TOptional<int>();
	}
	return CurrentConditionIdx + 1;
}

const TArray<USFCondition*> USFParticipant::GetAllConditions() const
{
	return Conditions;
}

int USFParticipant::GetCurrentConditionNumber() const
{
	return CurrentConditionIdx;
}

int USFParticipant::GetSequenceNumber() const
{
	return ParticipantSequenceNumber;
}

FString USFParticipant::GetID() const
{
	return ParticipantID;
}

bool USFParticipant::WasParticipantIdAlreadyUsed(FString NewParticipantID)
{
	//we highjack the independent variable file for this, since each participant adds entries to it
	FString Filename = FPaths::ProjectDir() + "StudyFramework/StudyLogs/IndependentVariables.csv";

	if (!FPaths::FileExists(Filename))
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::WasParticipantIdAlreadyUsed] IndependentVariables.csv does not exist (yet?)", false);
		return false;
	}

	FString StartStringSearch = NewParticipantID + ",";
	TArray<FString> ExistingLines;
	FFileHelper::LoadFileToStringArray(ExistingLines, *Filename);
	for (const FString& Line : ExistingLines)
	{
		if (Line.StartsWith(StartStringSearch))
		{
			return true;
		}
	}
	return false;
}

TArray<USFCondition*> USFParticipant::GetLastParticipantsConditions()
{
	TArray<USFCondition*> LoadedConditions;
	TMap<USFIndependentVariable*, FString> LoadedIndependentVariablesValues;
	ReadExecutionJsonFile(GetLastParticipantID(), LoadedConditions, LoadedIndependentVariablesValues);
	//Load WasStarted-Values from JSON, to ensure data is not overwritten without backups, if conditions are restarted
	TArray<int> LastParticipantsStartedConditionIndices = GetLastParticipantsStartedConditionIndices();
	for (int i = 0; i < LoadedConditions.Num(); i++)
	{
		LoadedConditions[i]->SetbStarted(LastParticipantsStartedConditionIndices.Contains(i));
	}
	return LoadedConditions;
}

int USFParticipant::GetLastParticipantSequenceNumber()
{
	TSharedPtr<FJsonObject> ParticipantJson = FSFUtils::ReadJsonFromFile("StudyRuns/LastParticipant.txt");
	if (ParticipantJson == nullptr)
	{
		//file does not exist or something else went wrong
		return -1;
	}
	return ParticipantJson->GetNumberField(TEXT("ParticipantSequenceNumber"));
}

FString USFParticipant::GetLastParticipantID()
{
	TSharedPtr<FJsonObject> ParticipantJson = FSFUtils::ReadJsonFromFile("StudyRuns/LastParticipant.txt");
	if (ParticipantJson == nullptr)
	{
		//file does not exist or something else went wrong
		return "";
	}
	return ParticipantJson->GetStringField(TEXT("ParticipantID"));
}

int USFParticipant::GetLastParticipantLastConditionStarted()
{
	TSharedPtr<FJsonObject> ParticipantJson = FSFUtils::ReadJsonFromFile("StudyRuns/LastParticipant.txt");
	if (ParticipantJson == nullptr)
	{
		//file does not exist or something else went wrong
		return -1;
	}
	return ParticipantJson->GetNumberField(TEXT("CurrentConditionIdx"));
}

TArray<int> USFParticipant::GetLastParticipantsStartedConditionIndices()
{
	TArray<int> StartedConditonIndices;
	TSharedPtr<FJsonObject> ParticipantJson = FSFUtils::ReadJsonFromFile("StudyRuns/LastParticipant.txt");
	if (ParticipantJson != nullptr)
	{
		TArray<TSharedPtr<FJsonValue>> StartedConditionsArray = ParticipantJson->GetArrayField(TEXT("StartedConditions"));
		for (auto& JsonValue : StartedConditionsArray)
		{
			TSharedPtr<FJsonObject> Entry = JsonValue->AsObject();
			if (Entry.IsValid() && Entry->GetBoolField(TEXT("WasStarted")))
			{
				StartedConditonIndices.Add(static_cast<int>(Entry->GetNumberField(TEXT("ConditionIndex"))));
			}
		}
	}
	return StartedConditonIndices;
}

bool USFParticipant::GetLastParticipantFinished()
{
	TSharedPtr<FJsonObject> ParticipantJson = FSFUtils::ReadJsonFromFile("StudyRuns/LastParticipant.txt");
	if (ParticipantJson == nullptr)
	{
		//file does not exist or something else went wrong
		return true;
	}
	return ParticipantJson->GetBoolField(TEXT("Finished"));
}

ASFStudySetup* USFParticipant::GetLastParticipantSetup()
{
	TSharedPtr<FJsonObject> ParticipantJson = FSFUtils::ReadJsonFromFile("StudyRuns/LastParticipant.txt");
	if (ParticipantJson == nullptr || !ParticipantJson->HasField(TEXT("StudySetup")))
	{
		//file does not exist or something else went wrong
		return nullptr;
	}
	FString SetupFile = ParticipantJson->GetStringField(TEXT("StudySetup"));
	ASFStudySetup* Setup = NewObject<ASFStudySetup>();
	Setup->JsonFile = SetupFile;
	Setup->LoadFromJson();

	return Setup;
}

void USFParticipant::LoadLastParticipantsIndependentVariables()
{
	TArray<USFCondition*> LoadedConditions;
	TMap<USFIndependentVariable*, FString> LoadedIndependentVariablesValues;
	ReadExecutionJsonFile(GetLastParticipantID(), LoadedConditions, LoadedIndependentVariablesValues);
	IndependentVariablesValues = LoadedIndependentVariablesValues;
}

const FString& USFParticipant::GetParticipantLoggingInfix() const
{
	return ParticipantLoggingInfix;
}

const TPair<USFIndependentVariable*, FString> USFParticipant::GetIndependentVariable(const FString& VarName)
{
	for (auto Elem : IndependentVariablesValues)
	{
		if (Elem.Key->Name == VarName)
		{
			return Elem;
		}
	}
	return TPair<USFIndependentVariable*, FString>(nullptr, "");
}

void USFParticipant::HandleClusterEvent(const FDisplayClusterClusterEventJson& Event)
{
	if (Event.Type == "SFParticipantEvent")
	{
		//now we actually react on all cluster nodes:
		if (Event.Name == "SetIndependentVariable")
		{
			SetIndependentVariableValue(Event.Parameters["VarName"], Event.Parameters["Value"]);
		}
	}
}

void USFParticipant::SetIndependentVariableValue(const FString& VarName, const FString& Value)
{
	bool updated = false;
	for (auto& Elem : IndependentVariablesValues)
	{
		if (Elem.Key->Name == VarName)
		{
			Elem.Value = Value;
			updated = true;
		}
	}
	if (updated && FSFUtils::IsPrimary())
	{
		USFLoggingBPLibrary::LogComment("Recorded new value for independent variable " + VarName + ": " + Value);
		UpdateIndependentVarsExecutionJsonFile();
		StoreInIndependentVarLongTable();
	}
}

void USFParticipant::SetIndependentVariablesFromStudySetup(ASFStudySetup* Setup)
{
	IndependentVariablesValues.Empty();
	for (auto Var : Setup->IndependentVariables)
	{
		IndependentVariablesValues.Add(DuplicateObject(Var, this), "");
		// secondary nodes do nothing, values will be added from cluster event
		if (!FSFUtils::IsPrimary())
		{
			continue;
		}
		FString Value = "";
		if (Var->bAskedAtBeginning)
		{
			switch (Var->ValueType)
			{
			case EValType::TEXT:
				{
					FString Answer;
					int Result = FSFUtils::OpenCustomDialogText(Var->Name, Var->Prompt, "", Answer);
					if (Result < 0)
					{
						FSFLoggingUtils::Log(
							"[USFParticipant::SetIndependentVariablesFromStudySetup] The window for the variable was closed without giving an answer!",
							false);
					}
					else
					{
						Value = Answer;
					}
				}
				break;

			case EValType::MULTIPLECHOICE:
				{
					int Answer = FSFUtils::OpenCustomDialog(Var->Name, Var->Prompt, Var->Options);
					if (Answer < 0)
					{
						FSFLoggingUtils::Log(
							"[USFParticipant::SetIndependentVariablesFromStudySetup] The window for the variable was closed without selecting anything!",
							false);
					}
					else
					{
						Value = Var->Options[Answer];
					}
				}
				break;
			}

			// Primary Node in cluster mode: Set variable by event for synchronization purposes
			if (IDisplayCluster::Get().GetOperationMode() == EDisplayClusterOperationMode::Cluster &&
				FSFUtils::IsPrimary())
			{
				IDisplayClusterClusterManager* const Manager = IDisplayCluster::Get().GetClusterMgr();
				FDisplayClusterClusterEventJson Event;
				TMap<FString, FString> Params;
				Params.Add("VarName", Var->Name);
				Params.Add("Value", Value);
				Event.Type = "SFParticipantEvent";
				Event.Name = "SetIndependentVariable";
				Event.Parameters = Params;
				Manager->EmitClusterEventJson(Event, true);
			}
			// Not in cluster mode: Directly set variable
			else
			{
				SetIndependentVariableValue(Var->Name, Value);
			}
		}
	}
}

bool USFParticipant::LoadConditionsFromJson()
{
	TArray<USFCondition*> LoadedConditions;
	TMap<USFIndependentVariable*, FString> LoadedIndependentVariablesValues;
	ReadExecutionJsonFile(GetLastParticipantID(), LoadedConditions, LoadedIndependentVariablesValues);
	Conditions = LoadedConditions;

	if (Conditions.Num() == 0)
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::LoadContitionsFromJson] No Conditions could be loaded for Participant " +
			ParticipantID, true);
		return false;
	}
	return true;
}

void USFParticipant::RecoverStudyResultsOfFinishedConditions()
{
	//this is not the most effective way of recovering but the most trivial (long tables will be read multiple times)
	for (USFCondition* Condition : Conditions)
	{
		const FString Filename = FPaths::ProjectDir() + "StudyFramework/StudyLogs/Phase_" + Condition->PhaseName +
			".csv";
		TArray<FString> Lines;
		if (!FFileHelper::LoadFileToStringArray(Lines, *Filename))
		{
			FSFLoggingUtils::Log(
				"[USFParticipant::RecoverStudyResultsOfFinishedConditions] Cannot read file: " + Filename +
				" (probably there was no data recorded yet).", false);
		}

		TArray<FString> HeaderEntries;
		if (Lines.Num() > 0)
		{
			Lines[0].ParseIntoArray(HeaderEntries, TEXT(","), false);
		}

		for (int i = 1; i < Lines.Num(); ++i)
		{
			TArray<FString> Entries;
			Lines[i].ParseIntoArray(Entries, TEXT(","), false);

			if (Entries.Num() > 0 && Entries[0] == ParticipantID)
			{
				Condition->RecoverStudyResults(HeaderEntries, Entries);
			}
		}

		//now also recover SFMultipleTrialDependentVariable data:
		for (USFDependentVariable* Var : Condition->DependentVariables)
		{
			if (auto MultiTrialVar = Cast<USFMultipleTrialDependentVariable>(Var))
			{
				MultiTrialVar->RecoverStudyResults(Condition, ParticipantID);
			}
		}
	}
}

void USFParticipant::CreateLongTableBackUp(const FString PathToSrcFile) const
{
	IFileManager& FileManager = IFileManager::Get();
	const FString ReplaceWith = "StudyFramework/RecyclingBin/" + CurrentBackUpFolderName + "/";
	const FString PathToDestFile = PathToSrcFile.Replace(TEXT("StudyFramework/StudyLogs/"), *ReplaceWith);
	FileManager.Copy(*PathToDestFile, *PathToSrcFile);
	FSFLoggingUtils::Log("Created Backup: " + PathToDestFile);
}

void USFParticipant::ClearLogData()
{
	const FString LongTableFolder = FPaths::ProjectDir() + "StudyFramework/StudyLogs/";
	const FString StudyRunsFolder = FPaths::ProjectDir() + "StudyFramework/StudyRuns";
	const FString RecyclingBinFolder = FPaths::ProjectDir() + "StudyFramework/RecyclingBin/";
	const FString Extension = "*.csv";
	const FString SearchPattern = LongTableFolder + Extension;
	const TArray<FString> LogSubfolders = {"ParticipantLogs", "GazeTrackingLogs", "PositionLogs"};
	TArray<FString> FileNames;
	IFileManager& FileManager = IFileManager::Get();
	FileManager.FindFiles(FileNames, *SearchPattern, true, false);
	//Instead of actually deleting files, we want to move them to a recycling bin folder, to minimize risk of data loss
	FString NewParentFolderPath = RecyclingBinFolder + "RestartStudyBackup-" + FDateTime::Now().ToString() + "/";
	//Move PhaseTables (.csv-files)
	for (FString Filename : FileNames)
	{
		const FString FullName = LongTableFolder + Filename;
		FileManager.Move(*(NewParentFolderPath + "StudyLogs/" + Filename), *FullName);
	}
	//Move execution-based log folders (PositionLogs, etc.)
	for (FString Subfolder : LogSubfolders)
	{
		const FString FullName = LongTableFolder + Subfolder;
		if (FileManager.DirectoryExists(*FullName))
		{
			FileManager.Move(*(NewParentFolderPath + "StudyLogs/" + Subfolder), *FullName);
		}
	}
	//Move StudyFramework/StudyRuns folder
	if (FileManager.DirectoryExists(*StudyRunsFolder))
	{
		FileManager.Move(*(NewParentFolderPath + "StudyRuns"), *StudyRunsFolder);
	}
	FSFLoggingUtils::Log("Moved .csv files: " + NewParentFolderPath);
}

void USFParticipant::SetCurrentBackUpFolderName(FString BackUpFolderName)
{
	CurrentBackUpFolderName = BackUpFolderName;
}


bool USFParticipant::SetNextCondition(int NewConditionIndx)
{
	if (NewConditionIndx < 0 || NewConditionIndx >= Conditions.Num())
	{
		FSFLoggingUtils::Log(
			"[USFParticipant::SetCondition()]: Requested condition (" + FString::FromInt(NewConditionIndx) +
			") out range (0 to " + FString::FromInt(Conditions.Num()) + ")!", true);
		return false;
	}

	NextConditionIdx = NewConditionIndx;
	return true;
}

bool USFParticipant::ApplyNextCondition()
{
	if (NextConditionIdx != -1)
	{
		if (GetCurrentCondition() && GetCurrentCondition()->WasStarted())
		{
			//we already ran a condition so store it
			if (GetCurrentCondition()->IsFinished())
			{
				StoreInPhaseLongTable();
			}
			else if (!Conditions[NextConditionIdx]->WasStarted())
			//otherwise we are restarting a condition, so obviously not finishing the current one
			{
				FSFLoggingUtils::Log(
					"[USFParticipant::SetCondition] Not storing unfinished last condition, when going to next. Make sure all required dependent variables received data!",
					false);
			}
		}

		CurrentConditionIdx = NextConditionIdx;
		LogCurrentParticipant();
		NextConditionIdx = -1;
		return true;
	}
	return false;
}

void USFParticipant::LogCurrentParticipant() const
{
	TSharedPtr<FJsonObject> Json = MakeShared<FJsonObject>();

	Json->SetNumberField("ParticipantSequenceNumber", ParticipantSequenceNumber);
	Json->SetStringField("ParticipantID", ParticipantID);
	bool bFinished = true;
	TArray<TSharedPtr<FJsonValue>> StartedConditions; //Cannot use TMap because we want to save to JSON
	for (int i = 0; i < Conditions.Num(); i++)
	{
		bFinished = bFinished && (Conditions[i]->IsFinished() || !Conditions[i]->HasRequiredVariables());
		TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();
		JsonObject->SetNumberField("ConditionIndex", i);
		JsonObject->SetBoolField("WasStarted", Conditions[i]->WasStarted());
		StartedConditions.Add(MakeShared<FJsonValueObject>(JsonObject));
	}
	Json->SetArrayField("StartedConditions", StartedConditions);
	Json->SetBoolField("Finished", bFinished);
	Json->SetNumberField("CurrentConditionIdx", CurrentConditionIdx);
	if (USFGameInstance::Get() && USFGameInstance::Get()->GetStudySetup())
	{
		Json->SetStringField("StudySetup", USFGameInstance::Get()->GetStudySetup()->JsonFile);
	}
	else
	{
		FSFLoggingUtils::Log("[USFParticipant::LogCurrentParticipant] StudySetup not accessible!", true);
	}


	FSFUtils::WriteJsonToFile(Json, "StudyRuns/LastParticipant.txt");
}
