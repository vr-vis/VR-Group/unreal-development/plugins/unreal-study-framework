#include "GazeTracking/SFGazeTracker.h"
#include "Logging/SFLoggingBPLibrary.h"
#include "SFGameInstance.h"
#include "SFSettings.h"
#include "GazeTracking/SFGazeTarget.h"
#include "GazeTracking/SFGazeTargetActor.h"
#include "Help/SFUtils.h"

#ifdef WITH_SRANIPAL
#include "SRanipalEye_FunctionLibrary.h"
#include "Eye/SRanipal_API_Eye.h"
#include "SRanipal_API.h"
#endif




bool USFGazeTracker::Tick(float DeltaTime)
{
	TimeSinceLastEyeDataGather += DeltaTime;
	if (TimeSinceLastEyeDataGather < EyeDataGatheringDelay)
	{
		return true;
	}

	TimeSinceLastEyeDataGather = 0.0f;

	//TODO:remove!
	//bool bSuccess = UEyeTrackerFunctionLibrary::GetGazeData(EyeTrackerGazeData);
	//USFLoggingBPLibrary::LogComment(FString("GetGazeDatawas ") + (bSuccess ? "Successfull" : "NOT successfull"));
	//return true;

	//We should not start another Async task, if the first has not terminated yet
	if (bIsAsyncEyeTrackingTaskRunning)
	{
		//USFLoggingBPLibrary::LogComment("Missed eye update due to slow async execution");
	}
	else if (bEyeTrackingStarted)
	{
		bIsAsyncEyeTrackingTaskRunning = true;
		if (GazeTrackingBackend == EGazeTrackingBackend::SRanipal) {
#ifdef WITH_SRANIPAL
			//transfer 'this' into the AnyThread-task and pass it to the GameThread-task in order to have access to bDataLogged, bIsAsyncEyeTrackingTaskRunning, SranipalEyeData there
			AsyncTask(ENamedThreads::AnyThread, [this]()
				{
					ViveSR::anipal::Eye::EyeData_v2 LocalEyeData;
					int Result = ViveSR::anipal::Eye::GetEyeData_v2(&LocalEyeData);
					// Set member variables from GameThread to avoid race conditions
					AsyncTask(ENamedThreads::GameThread, [this, LocalEyeData = MoveTemp(LocalEyeData), Result]() mutable
						{
							if (Result != 0)
							{
								USFLoggingBPLibrary::LogComment("GetEyeData_v2 ERROR: " + FString::FromInt(Result) + " (see ViveSR::Error for coding). Did not update SranipalEyeData.");
							}
							else
							{
								SranipalEyeData = LocalEyeData;
								bDataLogged = false;
							}
							bIsAsyncEyeTrackingTaskRunning = false;
						});
				});
#endif
		}
		else if (GazeTrackingBackend == EGazeTrackingBackend::OpenXREyeTracker) {
			AsyncTask(ENamedThreads::AnyThread, [this]()
				{
					FEyeTrackerGazeData LocalGazeData;
					bool bSuccess = UEyeTrackerFunctionLibrary::GetGazeData(LocalGazeData);
					// Set member variables from GameThread to avoid race conditions
					AsyncTask(ENamedThreads::GameThread, [this, LocalGazeData = MoveTemp(LocalGazeData), bSuccess]() mutable
						{
							if (!bSuccess)
							{
								USFLoggingBPLibrary::LogComment("UEyeTrackerFunctionLibrary::GetGazeData() was not successful, gaze data was not updated.");
							}
							else
							{
								EyeTrackerGazeData = LocalGazeData;
								bDataLogged = false;
							}
							bIsAsyncEyeTrackingTaskRunning = false;
						});
				});
		}
	}
	return true;
}

void USFGazeTracker::Init(EGazeTrackerMode Mode, bool IgnoreNonGazeTargetActors, float DataGatheringsPerSecond, EGazeTrackingBackend BackendToUse)
{
	bIgnoreNonGazeTargetActors = IgnoreNonGazeTargetActors;
	bEyeTrackingStarted = false;

#ifndef WITH_SRANIPAL
	if (BackendToUse == EGazeTrackingBackend::SRanipal) {
		FSFUtils::OpenMessageBox("Cannot use SRanipal as gaze tracking backend, since the plugin is not present. Fallback to HeadRotationOnly", true);
		Mode = EGazeTrackerMode::HeadRotationOnly;
	}
#endif


	if (Mode == EGazeTrackerMode::EyeTracking)
	{
		switch (GazeTrackingBackend)
		{
		case EGazeTrackingBackend::SRanipal:
#ifdef WITH_SRANIPAL
			if (ViveSR::anipal::Eye::IsViveProEye())
			{
				USRanipalEye_FunctionLibrary::StartEyeFramework(SupportedEyeVersion::version2);
				bEyeTrackingStarted = true;
			}
			else
			{
				USFLoggingBPLibrary::LogComment("No Vive Pro Eye present, use head rotation only for gaze tracking.", true);
			}
#endif
			break;
		case EGazeTrackingBackend::OpenXREyeTracker:
			if (UEyeTrackerFunctionLibrary::IsEyeTrackerConnected()) {
				bEyeTrackingStarted = true;
			}
			else {
				USFLoggingBPLibrary::LogComment("No EyeTracker is connected, use head rotation only for gaze tracking. Maybe enable OpenXREyeTracking plugin?", true);
			}
			break;
		default:
			FSFUtils::OpenMessageBox("Unknown EGazeTrackingBackend used in USFGazeTracker::Init()", true);
			break;
		}

		GazeTrackingBackend = BackendToUse;

		TimeSinceLastEyeDataGather = 0.0f;
		if (DataGatheringsPerSecond > 0.0f) {
			EyeDataGatheringDelay = 1.0f / DataGatheringsPerSecond;
		}
		else
		{
			EyeDataGatheringDelay = 0.0f;
		}
	}

	TickDelegate = FTickerDelegate::CreateUObject(this, &USFGazeTracker::Tick);
	TickDelegateHandle = FTSTicker::GetCoreTicker().AddTicker(TickDelegate);

}

FGazeRay USFGazeTracker::GetLocalGazeDirection()
{
	if (!bEyeTrackingStarted)
	{
		// if no eye tracker is used we always "look ahead"
		return FGazeRay();
	}

	if (GazeTrackingBackend == EGazeTrackingBackend::SRanipal) {
		return GetSranipalGazeRayFromData();
	}
	else if (GazeTrackingBackend == EGazeTrackingBackend::OpenXREyeTracker) {
		//TODO: should not be used?
		return GetGazeRayFromEyeTrackerData();
	}

	return FGazeRay();
}

FGazeRay USFGazeTracker::GetWorldGazeDirection()
{
	if (bEyeTrackingStarted && GazeTrackingBackend == EGazeTrackingBackend::OpenXREyeTracker) {
		//this is already in world coordinates
		return GetGazeRayFromEyeTrackerData();
	}

	//otherwise take local and transform it into world space
	FGazeRay LocalGazeRay = GetLocalGazeDirection();

	UWorld* World = USFGameInstance::Get()->GetWorld();
	if (!World->GetFirstPlayerController()) {
		return FGazeRay();
	}

	//the gaze ray is relative to the HMD
	const APlayerCameraManager* CamManager = World->GetFirstPlayerController()->
		PlayerCameraManager;
	const FVector CameraLocation = CamManager->GetCameraLocation();
	const FRotator CameraRotation = CamManager->GetCameraRotation();

	FGazeRay GazeRay;
	GazeRay.Origin = CameraLocation + CameraRotation.RotateVector(LocalGazeRay.Origin);
	GazeRay.Direction = CameraRotation.RotateVector(LocalGazeRay.Direction);

	return GazeRay;
}

FString USFGazeTracker::GetCurrentGazeTarget()
{
	FString GazedAtTarget = "";
	const float Distance = 10000.0f;

	FGazeRay GazeRay = GetWorldGazeDirection();

	const FVector RayCastOrigin = GazeRay.Origin;
	const FVector RayCastEnd = (GazeRay.Direction * Distance) + RayCastOrigin;

	//FSFUtils::Log("Cast Ray from "+RayCastOrigin.ToString()+" to "+RayCastEnd.ToString());

	FHitResult HitResult;
	UWorld* World = USFGameInstance::Get()->GetWorld();
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(World->GetFirstPlayerController()->AcknowledgedPawn);
	QueryParams.AddIgnoredActor(USFGameInstance::Get()->GetHUD()->GetHUDHelper());
	ActorsToIgnore.Remove(nullptr);//remove all invalid actors first
	QueryParams.AddIgnoredActors(ActorsToIgnore);

	if (bDebugRenderRayTraces)
	{
		//this line trace is more comfortable for debug drawing, however has problems with channel tracing, so we use LineTraceSingleByChannel() above
		FHitResult TmpHitRes;
		UKismetSystemLibrary::LineTraceSingle(World, RayCastOrigin, RayCastEnd, ETraceTypeQuery::TraceTypeQuery1, false, {}, EDrawDebugTrace::ForDuration, TmpHitRes, true);
	}

	while (true) {
		World->LineTraceSingleByChannel(HitResult, RayCastOrigin, RayCastEnd, ECC_Visibility, QueryParams);

		if (HitResult.bBlockingHit && HitResult.GetActor())
		{
			//we hit something check whether the hit component is one of our SFGazeTarget components
			USFGazeTarget* GazeTarget = Cast<USFGazeTarget>(HitResult.GetComponent());
			if (GazeTarget)
			{
				return GazeTarget->GetTargetName();
			}

			//otherwise the whole actor might be a target:
			if (HitResult.GetActor())
			{
				USFGazeTargetActor* GazeTargetActor = Cast<USFGazeTargetActor>(HitResult.GetActor()->GetComponentByClass(USFGazeTargetActor::StaticClass()));
				if (GazeTargetActor)
				{
					return GazeTargetActor->GetTargetName();
				}
			}
			if (!bIgnoreNonGazeTargetActors)
			{
				//if we do not want to ignore non-gazetarget actors we are done here after running this loop once
				return "";
			}
			ActorsToIgnore.Add(HitResult.GetActor());
			QueryParams.AddIgnoredActor(HitResult.GetActor());
		}
		else
		{
			break;
		}
	}



	return "";
}

bool USFGazeTracker::LaunchCalibration()
{
	if (GazeTrackingBackend == EGazeTrackingBackend::SRanipal) {
#ifdef WITH_SRANIPAL
		int Result = ViveSR::anipal::Eye::LaunchEyeCalibration(nullptr);
		if (Result == ViveSR::Error::WORK)
		{
			USFLoggingBPLibrary::LogComment("Successful eye claibration (SRanipal).");
			return true;
		}
		USFLoggingBPLibrary::LogComment("Eye Tracking Calibration failed with error code: " + FString::FromInt(Result) + " (see ViveSR::Error for coding).");
		return false;
#endif
	}
	else if (GazeTrackingBackend == EGazeTrackingBackend::OpenXREyeTracker) {
		const USFSettings* Settings = GetDefault<USFSettings>();
		FString CalibrationExe = Settings->EyeCalibrationExePath;

		if (!FPaths::FileExists(CalibrationExe)) {
			FSFUtils::OpenMessageBox("Gaze Calibration for OpenXR Eye Tracking cannot be started the file specified in the settings (Project Settings -> StudyFramework -> Eye Calibration Path) does not exist!", true);
			return false;
		}
		/*void* ReadPipe;
		void* WritePipe;
		FPlatformProcess::CreatePipe(ReadPipe, WritePipe);
		FPlatformProcess::CreateProc(*CalibrationExe, nullptr, false, false, false, nullptr, 0, nullptr, WritePipe, ReadPipe);
		FString Output = FPlatformProcess::ReadPipe(ReadPipe);
		FString Output2 = FPlatformProcess::ReadPipe(WritePipe);
		UE_LOG(LogTemp, Warning, TEXT("Calibration output was: %s and %s"), *Output, *Output2);*/
		FPlatformProcess::CreateProc(*CalibrationExe, nullptr, false, false, false, nullptr, 0, nullptr, nullptr, nullptr);
	}

	USFLoggingBPLibrary::LogComment("Eye Tracking Calibration cannot be performed, gaze tracking backend unknown.");
	return false;
}

bool USFGazeTracker::IsTrackingEyes()
{
	if (!bEyeTrackingStarted)
		return false;

	if (GazeTrackingBackend == EGazeTrackingBackend::SRanipal) {
#ifdef WITH_SRANIPAL
		//no_user apparently is true, when a user is there... (weird but consistent)
		return SranipalEyeData.no_user;
#endif
	}
	else if (GazeTrackingBackend == EGazeTrackingBackend::OpenXREyeTracker) {
		IEyeTracker const* const ET = GEngine ? GEngine->EyeTrackingDevice.Get() : nullptr;
		if (ET)
		{
			EEyeTrackerStatus const Status = ET->GetEyeTrackerStatus();
			return (Status == EEyeTrackerStatus::Tracking);
		}
		return false;
	}

	return false;
}

float USFGazeTracker::GetEyesOpenness()
{
	if (!IsTrackingEyes())
	{
		return -1.0f;
	}

	if (GazeTrackingBackend == EGazeTrackingBackend::SRanipal) {
#ifdef WITH_SRANIPAL
		return 0.5f * (SranipalEyeData.verbose_data.left.eye_openness + SranipalEyeData.verbose_data.right.eye_openness);
#endif
	}
	else if (GazeTrackingBackend == EGazeTrackingBackend::OpenXREyeTracker) {
		return (EyeTrackerGazeData.bIsLeftEyeBlink || EyeTrackerGazeData.bIsRightEyeBlink) ? 0.0f : 1.0f;
	}

	return -1.0f;
}

bool USFGazeTracker::DataAlreadyLogged()
{
	return bDataLogged;
}

void USFGazeTracker::SetDataLogged()
{
	bDataLogged = true;
}

float USFGazeTracker::GetPupilDiameter()
{
	if (!IsTrackingEyes())
	{
		return 0.0f;
	}
	if (GazeTrackingBackend == EGazeTrackingBackend::SRanipal) {
#ifdef WITH_SRANIPAL
		return 0.5f * (SranipalEyeData.verbose_data.left.pupil_diameter_mm + SranipalEyeData.verbose_data.right.pupil_diameter_mm);
#endif
	}
	else if (GazeTrackingBackend == EGazeTrackingBackend::OpenXREyeTracker) {
		return 0.5f * (EyeTrackerGazeData.LeftPupilDiameter + EyeTrackerGazeData.RightPupilDiameter);
	}

	return 0.0f;
}

FGazeRay USFGazeTracker::GetSranipalGazeRayFromData()
{
	FGazeRay GazeRay;
#ifdef WITH_SRANIPAL
	//this is copied from SRanipalEye_Core.cpp GetGazeRay() (case gazeIndex == GazeIndex::COMBINE)
	bool bValid = SranipalEyeData.verbose_data.left.GetValidity(ViveSR::anipal::Eye::SingleEyeDataValidity::SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY)
		&& SranipalEyeData.verbose_data.right.GetValidity(ViveSR::anipal::Eye::SingleEyeDataValidity::SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY);
	if (bValid)
	{
		FVector SranipalOrigin = (SranipalEyeData.verbose_data.left.gaze_origin_mm + SranipalEyeData.verbose_data.right.gaze_origin_mm) / 1000 / 2;
		FVector SRanipalDirection = (SranipalEyeData.verbose_data.left.gaze_direction_normalized + SranipalEyeData.verbose_data.right.gaze_direction_normalized) / 2;

		//CovertToUnrealLocation(origin)
		GazeRay.Origin.X = SranipalOrigin.Z;
		GazeRay.Origin.Y = SranipalOrigin.X * -1;
		GazeRay.Origin.Z = SranipalOrigin.Y;
		//ApplyUnrealWorldToMeterScale(origin);
		float Scale = GWorld ? GWorld->GetWorldSettings()->WorldToMeters : 100.f;
		GazeRay.Origin *= Scale;
		//CovertToUnrealLocation(direction);
		GazeRay.Direction.X = SRanipalDirection.Z;
		GazeRay.Direction.Y = SRanipalDirection.X;
		GazeRay.Direction.Z = SRanipalDirection.Y;
	}
#endif
	return GazeRay;
}

FGazeRay USFGazeTracker::GetGazeRayFromEyeTrackerData()
{
	FGazeRay GazeRay;
#ifndef WITH_SRANIPAL
	GazeRay.Origin = EyeTrackerGazeData.GazeOrigin;
	GazeRay.Direction = EyeTrackerGazeData.GazeDirection;
#endif
	return GazeRay;
}
